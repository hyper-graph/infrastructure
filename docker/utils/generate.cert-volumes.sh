cd /opt/certs

function prepareRabbitmqVolume() {
    cd rabbitmq
    cp ca.crt   /opt/rabbitmq/ca.crt

    cd server
    cp cert.pem /opt/rabbitmq/cert.pem
    cp key.pem  /opt/rabbitmq/key.pem

    chmod 644 /opt/rabbitmq/key.pem
}

prepareRabbitmqVolume

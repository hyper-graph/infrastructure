set -e

HOST=$1
OUTPUT_PATH=$2
CERT_SUBJ="/C=RU/ST=Moscow/L=Moscow/O=Hyper Graph/OU=IT/CN=$HOST"

openssl req -x509 -out "$OUTPUT_PATH/ca.crt" -keyout "$OUTPUT_PATH/ca.key" \
  -newkey rsa:2048 -nodes -sha256 \
  -subj "/CN=$HOST" \
	-subj "$CERT_SUBJ"

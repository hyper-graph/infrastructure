set -e

HOST=$1
CERT_SUBJ="/C=RU/ST=Moscow/L=Moscow/O=Hyper Graph/OU=IT/CN=$HOST"
PATH_TO_RABBITMQ_CERTS=/opt/certs/rabbitmq

mkdir -p $PATH_TO_RABBITMQ_CERTS

function generate() {
    CERT_TYPE=$1
		PATH_TO_CERTS=$PATH_TO_RABBITMQ_CERTS/$CERT_TYPE

		mkdir -p $PATH_TO_CERTS

		openssl genrsa -out "$PATH_TO_CERTS/key.pem" 4096

		openssl req -new -key "$PATH_TO_CERTS/key.pem" -out "$PATH_TO_CERTS/req.csr" -subj "$CERT_SUBJ"

		openssl x509 -req \
			-in "$PATH_TO_CERTS/req.csr" \
			-CA "$PATH_TO_RABBITMQ_CERTS/ca.crt" \
			-CAkey "$PATH_TO_RABBITMQ_CERTS/ca.key" \
			-CAcreateserial -days 365 -sha256 \
			-out "$PATH_TO_CERTS/cert.pem" \
			-extfile "rabbitmq.$CERT_TYPE.cnf"
}
./generate-ca.sh $HOST $PATH_TO_RABBITMQ_CERTS
generate client
generate server

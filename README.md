# Hyper graph infrastructure
This repository contains docker compose for run infrastructure services and scripts.

## Running
For run docker compose copy [env file](.env.sample) to `.env`.

After that you can run docker compose.

```shell
[sudo] docker-compose -p hg up -d
```
